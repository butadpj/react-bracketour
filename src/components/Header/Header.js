import React from 'react';
import './Header.css';

const Header = () => {
  return (
    <header>
      <h1 className="main__title">Bracketour</h1>
      <h4 className="sub__title">Create brackets for tournament</h4>
    </header>
  )
}

export default Header
